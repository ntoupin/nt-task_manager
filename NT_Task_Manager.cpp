/*
 * @file NT_Task_Manager.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Task manager function definitions.
 */

#include "Arduino.h"
#include "NT_Task_Manager.h"

Task_t::Task_t()
{
}

void Task_t::Configure(int Period, Function_t Function)
{
	_Period = Period;
	_Function = Function;
}

void Task_t::Init()
{
	Enable = TRUE;
}

void Task_t::Deinit()
{
	Enable = FALSE;
}

void Task_t::Run()
{
	// Capture current timestamp
	long Current_Timetimestamp = millis();

	// Execute function if time is due
	if ((Current_Timetimestamp - _Last_Execution) >= _Period)
	{
		// Execute linked function
		_Function();

		// Save timestamp of current execution
		_Last_Execution = Current_Timetimestamp;
	}
}