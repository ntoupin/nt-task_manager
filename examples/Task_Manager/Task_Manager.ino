/*
   @file Task_Manager.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of task manager utilisation.
*/

#include <NT_Task_Manager.h>

// Create task object
Task_t Task_1;

// Custom function
bool Led_State = FALSE;
void Function()
{
  // Toogle DEL
  Led_State = !Led_State;
  digitalWrite(LED_BUILTIN, Led_State);
}

void setup()
{
  // Configure initial parameters (param1 : Period, param2 : Function)
  Task_1.Configure(100, Function);

  // Enable the task
  Task_1.Init();

  // Set pin mode
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  // Run task
  Task_1.Run();
}
