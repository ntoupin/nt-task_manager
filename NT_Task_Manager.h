/*
 * @file NT_Task_Manager.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Task manager function declarations.
 */

#ifndef NT_Task_Manager_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Task_Manager_h

class Task_t
{
public:
	Task_t();
	typedef void(*Function_t)();
	bool Enable = FALSE;
	void Configure(int Period, Function_t Function);
	void Init();
	void Deinit();
	void Run();	

private:
	int _Period = 0;
	int _Last_Execution = 0;
	Function_t _Function;
};

#endif